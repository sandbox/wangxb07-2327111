<?php

/**
 * @file
 * Implements Tenpay in Drupal Commerce.
 */

/**
 * Define trade mode const
 */
define('COMMERCE_TENPAY_TRADE_MODE_DIRECT', '1');
define('COMMERCE_TENPAY_TRADE_MODE_ESCROW', '2');
define('COMMERCE_TENPAY_TRADE_MODE_DUALFUN', '3');

/**
 * Define trade status const
 */
define('COMMERCE_TENPAY_TRADE_STATUS_PAYSUCC', 0); // 0付款成功
define('COMMERCE_TENPAY_TRADE_STATUS_CREATED', 1); // 1交易创建
define('COMMERCE_TENPAY_TRADE_STATUS_DELIVERY_INFO', 2); // 2收获地址填写完毕
define('COMMERCE_TENPAY_TRADE_STATUS_SEND_GOODS', 4); // 4卖家发货成功
define('COMMERCE_TENPAY_TRADE_STATUS_CONFIRM_GOODS', 5); // 5买家收货确认，交易成功
define('COMMERCE_TENPAY_TRADE_STATUS_CONFIRM_CLOSE_OUTDATE', 6); // 6交易关闭，未完成超时关闭
define('COMMERCE_TENPAY_TRADE_STATUS_CONFIRM_TOTLE_FEE_CHANGED', 7); // 7修改交易价格成功
define('COMMERCE_TENPAY_TRADE_STATUS_CONFIRM_REFUND', 8); // 8买家发起退款
define('COMMERCE_TENPAY_TRADE_STATUS_CONFIRM_REFUND_SUCC', 9); // 9退款成功
define('COMMERCE_TENPAY_TRADE_STATUS_CONFIRM_REFUND_CLOSE', 10); // 10退款关闭

/**
 * Implements hook_menu()
 */
function commerce_tenpay_menu() {
  $items['commerce_tenpay/notify'] = array(
    'page callback' => 'commerce_tenpay_process_notify',
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Implements hook_commerce_payment_method_info().
 */
function commerce_tenpay_commerce_payment_method_info() {
  $payment_methods = array();

  $payment_methods['tenpay'] = array(
    'base' => 'commerce_tenpay',
    'title' => t('Tenpay'),
    'terminal' => FALSE,
    'offsite' => TRUE,
    'offsite_autoredirect' => TRUE,
  );

  return $payment_methods;
}

/**
 * Payment method callback: settings form.
 */
function commerce_tenpay_settings_form($settings = NULL) {
  $form = array();

  $settings = (array) $settings + array(
    'partner' => '',
    'key' => '',
    'trade_mode' => 1,
    'debug' => '',
  );

  $form['partner'] = array(
    '#type' => 'textfield',
    '#title' => t('App ID'),
    '#description' => t('The app id you use for the Tenpay account you want to receive payments.'),
    '#default_value' => $settings['partner'],
    '#required' => TRUE,
  );
  $form['key'] = array(
    '#type' => 'textfield',
    '#title' => t('Key'),
    '#description' => t('The key code'),
    '#default_value' => $settings['key'],
    '#required' => TRUE,
  );

  $form['trade_mode'] = array(
    '#type' => 'select',
    '#title' => t('Trade mode'),
    '#description' => t('The service trade mode'),
    '#default_value' => $settings['trade_mode'],
    '#options' => array(
      COMMERCE_TENPAY_TRADE_MODE_DIRECT => t('Direct'),
      COMMERCE_TENPAY_TRADE_MODE_ESCROW => t('Escrow'),
      COMMERCE_TENPAY_TRADE_MODE_DUALFUN => t('DualFun'),
    ),
    '#required' => TRUE,
  );
  $form['debug'] = array(
    '#type' => 'checkbox',
    '#title' => t('Debug'),
    '#description' => t('0.01 pay test.'),
    '#default_value' => $settings['debug'],
  );

  return $form;
}

/**
 * Payment method callback: adds a message to the submission form.
 */
function commerce_tenpay_submit_form($payment_method, $pane_values, $checkout_pane, $order) {
  $form['tenpay_information'] = array(
    '#markup' => '<span class="commerce-tenpay-info">' . t('(Continue with checkout to complete payment via Tenpay.)') . '</span>',
  );

  $form['bank_type'] = array(
    '#type' => 'radios',
    '#title' => t('Bank type'),
    '#defualt_value' => '0',
    '#process' => array('form_process_radios', 'commerce_tenpay_process_banktype_radios'),
    '#options' => array(
      '1001' => t('CMBCHINA'),
      '1002' => t('ICBC'),
      '1003' => t('CCB'),
      '1004' => t('SPDB'),
      '1005' => t('ABCHINA'),
      '1006' => t('CMBC'),
      '1008' => t('SDB'),
      '1009' => t('CIB'),
      '1010' => t('PINGAN'),
      '1020' => t('BANKCOMM'),
      '1021' => t('ECITIC'),
      '1022' => t('CEBBANK'),
      '1023' => t('CGBCHINA'),
      '1032' => t('bankofbeijing'),
      '1052' => t('BOC'),
      '0' => t('tenpay'),
    ),
    '#required' => TRUE,
  );
  $form['#attached']['css'] = array(
    drupal_get_path('module', 'commerce_tenpay') . '/commerce_tenpay.css',
  );

  return $form;
}

/**
 * Payment method callback: submit form submit
 */
function commerce_tenpay_submit_form_submit($payment_method, $pane_form, $pane_values, $order, $charge) {
  global $language;
  if ($payment_method['method_id'] == 'tenpay' && isset($pane_values['bank_type'])) {
    $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
    $order_wrapper->commerce_order_bank_type->set($pane_values['bank_type']);
    $order_wrapper->save();
  }
}

/**
 * Payment method callback: redirect form.
 */
function commerce_tenpay_redirect_form($form, &$form_state, $order, $payment_method) {
  // Return an error if the enabling action's settings haven't been configured.
  if (empty($payment_method['settings']['partner'])) {
    drupal_set_message(t('Tenpay is not configured for use. No APP ID has been specified.'), 'error');
    return array();
  }
  $form = array();
  $wrapper = entity_metadata_wrapper('commerce_order', $order);
  $currency_code = $wrapper->commerce_order_total->currency_code->value();
  $amount = $payment_method['settings']['debug'] ? 1 : $wrapper->commerce_order_total->amount->value();

  // Set feedback URLs.
  $settings = array(
    // Return to the payment redirect page for processing successful payments.
    'return' => url('checkout/' . $order->order_id . '/payment/return/' . $order->data['payment_redirect_key'], array('absolute' => TRUE)),
    'notify' => url('commerce_tenpay/notify', array('absolute' => TRUE)),
  );

  $settings = $payment_method['settings'] + $settings;

  $data = array(
    'partner' => $settings['partner'],
    'out_trade_no' => $order->order_number,
    'total_fee' => $amount,
    'notify_url' => $settings['notify'],
    'return_url' => $settings['return'],
    'body' => commerce_tenpay_data_body($order),
    'bank_type' => $wrapper->commerce_order_bank_type->value(),
    'spbill_create_ip' => $_SERVER['REMOTE_ADDR'],
    'fee_type' => "1",
    'subject' => 'product name', // 商品名称
    'sign_type' => 'MD5',
    'service_version' => '1.0',
    'input_charset' => 'utf-8',
    'sign_key_index' => '1',
    // 以下都是可选
    'attach' => '', //附件数据，原样返回就可以了
    'product_fee' => '', //商品费用
    'transport_fee' => '0', //物流费用
    'time_start' => date("YmdHis"), //订单生成时间
    'time_expire' => '', //订单失效时间
    'buyer_id' => '', //买方财付通帐号
    'goods_tag' => '', //商品标记
    'trade_mode' => $settings['trade_mode'], //交易模式（1.即时到帐模式，2.中介担保模式，3.后台选择（卖家进入支付中心列表选择））
    'transport_desc' => '', //物流说明
    'trans_type' => '1', //交易类型
    'agentid' => '', //平台ID
    'agent_type' => '', //代理模式（0.无代理，1.表示卡易售模式，2.表示网店模式）
    'seller_id' => '', //卖家的商户号
  );

  $data['sign'] = commerce_tenpay_build_sign($settings['key'], $data);

  $form['#token'] = FALSE;
  foreach ($data as $name => $value) {
    if (!empty($value)) {
      $value = trim($value);
      // Add the value as a hidden form element.
      $form[$name] = array('#type' => 'hidden', '#value' => $value);
    }
  }


  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Proceed with payment'),
    '#submit' => array('commerce_tenpay_redirect'),
  );
  return $form;
}

/**
 * Redirect to tenpay.
 */
function commerce_tenpay_redirect($form, &$form_state) {
  $url = 'https://api.tenpay.com/gateway/pay.htm?';
  form_state_values_clean($form_state);
  $data = commerce_tenpay_build_query($form_state['values']);
  watchdog('commerce_tenpay', 'redirect to payment page: %url', array('%url' => $url . $data), WATCHDOG_INFO);
  $form_state['redirect'] = $url . $data;
}

/**
 * build sign by secret and request data
 * @param $secret
 * @param $data
 * @return md5 string
 */
function commerce_tenpay_build_sign($secret, $data) {
  $query = commerce_tenpay_build_query($data, TRUE);
  $full_query = $query . "&key=" . $secret;
  return strtolower(md5($full_query));
}

/**
 * format request query for tenpay
 * @param $data
 * @return
 * request query string
 */
function commerce_tenpay_build_query($data, $for_sign = FALSE) {
  $query = "";
  ksort($data);
  foreach ($data as $k => $v) {
    if ($k == "sign" && $for_sign) {
      continue;
    }
    if (!empty($v) && $v != "null") {
      if(!$for_sign) {
        $v = urlencode($v);
      }
      $query .= $k . "=" . $v . "&";
    }
  }

  if (strlen($query) > 0) {
    return substr($query, 0, strlen($query)-1);
  }
  else {
    return '';
  }
}

/**
 * Callback for body description.
 */
function commerce_tenpay_data_body($order) {
  if (empty($order)) {
    return '';
  }
  else {
    $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
    $line_items = $order_wrapper->commerce_line_items->value();
    foreach ($line_items as $line_item) {
      if ($line_item->type == 'product' && !empty($line_item->commerce_product)) {
        $line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);
        $body[] = $line_item_wrapper->commerce_product->title->value();
      }
    }
    return implode($body, ' | ');
  }
}

/**
 * Menu callback function to process Tenpay's feedback notifications.
 */
function commerce_tenpay_process_notify() {
  if (empty($_REQUEST)) {
    return FALSE;
  }
  $order = commerce_order_load($_REQUEST['out_trade_no']);
  $payment_method = commerce_payment_method_instance_load($order->data['payment_method']);
  // Validate the received notification from Tenpay.
  if (commerce_tenpay_notify_validate($order, $payment_method, $_REQUEST)) {
    commerce_tenpay_notify_submit($order, $payment_method, $_REQUEST);
  }
}

/**
 * Validation of Alipay's notifications.
 */
function commerce_tenpay_notify_validate($order, $payment_method, $notify) {
  if (empty($notify)) {
    return FALSE;
  }
  // Log an entry of the notification received for a transaction.
  watchdog('commerce_tenpay', 'Customer returned from Tenpay with the following data:<pre>@notify</pre>', array('@notify' => print_r($notify, TRUE)));
  $notify = drupal_get_query_parameters($notify);
  // Encrypted transaction signature.
  $sign = commerce_tenpay_build_sign($payment_method['settings']['key'], $notify);
/*
  if ($sign != $notify['sign']) {
    watchdog('commerce_tenpay', 'Sign validate failed: source sign is @source_sign, notify sign is @notify_sign', array('@source_sign' => $sign, '@notify_sign' => $notify['sign']));
    return FALSE;
  }*/
  return TRUE;
  // commerce_tenpay_notify_submit($order, $payment_method, $notify);
/**
  $notify_response = commerce_tenpay_notify_request($payment_method, $notify);
  if ($notify_response) {
    watchdog('commerce_tenpay', 'Notify request returned data:<pre>@notify</pre>', array('@notify' => var_export($notify_response, TRUE)));
    return TRUE;
  }
  else {
    drupal_set_message(t('Fetch data by notify id failed'), 'error');
    watchdog('commerce_tenpay', 'Notify request failed');
    return FALSE;
  }
*/
}

/**
 * Send request to notify-more-information server, and get response.
 */
function commerce_tenpay_notify_request($payment_method, $notify) {
  $data['partner'] = $payment_method['settings']['partner'];
  $data['notify_id'] = $notify['notify_id'];

  $data['sign'] = commerce_tenpay_build_sign($payment_method['settings']['key'], $data);

  $verify_url = extension_loaded('openssl') ? "https://gw.tenpay.com/gateway/simpleverifynotifyid.xml?" : "http://gw.tenpay.com/gateway/simpleverifynotifyid.xml?";
  return drupal_http_request($verify_url . drupal_http_build_query($data));
}

/**
 * Order status helper function
 */
function commerce_tenpay_notify_submit($order, $payment_method, $notify) {
  // Attempt to load prior authorization capture IPN created previously.
  $transactions = commerce_payment_transaction_load_multiple(array(), array('remote_id' => $notify["transaction_id"]));
  if (!empty($transactions)) {
    // @TODO: Load the prior transaction ID and update with the capture values.
    // There is probably some special handling necessary in this case.
    $transaction = reset($transactions);
  }
  else {
    // Create a new payment transaction for the order.
    $transaction = commerce_payment_transaction_new($payment_method['method_id'], $order->order_id);
    $transaction->instance_id = $payment_method['instance_id'];
  }
  // Currently supports only transactions in CNY.
  $transaction->amount = commerce_currency_decimal_to_amount($notify['total_fee'], 'CNY');
  $transaction->currency_code = 'CNY';

  $transaction->remote_id = $notify["transaction_id"];
  // Set the transaction's statuses based on notify's trade_status.
  $transaction->remote_status = $notify['trade_state'];
  $transaction->data['notify'] = $notify;

  // Handle refund types of cases.
  if (isset($notify['refund_status'])) {
    // TODO handle refund.
  }
  else {
    // Handle trade types of cases.
    switch ($notify['trade_status']) {
      // Pay succecss
      //TODO 目前只完成了即时到账的接口
      case COMMERCE_TENPAY_TRADE_STATUS_PAYSUCC:
        $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
        $transaction->message = commerce_tenpay_status_message($notify['trade_status']);
        commerce_order_status_update($order, 'wait_send_goods');
        break;
    }
  }
  // Save the payment transaction and redirect to next page.
  commerce_payment_transaction_save($transaction);
  commerce_payment_redirect_pane_next_page($order);
  // Record an entry of the transaction with the order and trade numbers.
  watchdog('commerce_tenpay', 'Notify processed for Order @order_number with ID @trade_no.', array('@trade_no' => $notify["transaction_id"], '@order_number' => $order->order_number), WATCHDOG_INFO);
  echo 'success';
}

/**
 * status message builder
 */
function commerce_tenpay_status_message($status) {
  switch ($status) {
    // Messages used for refund types of cases.
    case COMMERCE_TENPAY_TRADE_STATUS_PAYSUCC:
      return t('The payment has completed.');
  }
}

/**
 * Implements new radios theme for tenpay bank type
 */
function commerce_tenpay_process_banktype_radios($element) {
  if (count($element['#options']) > 0) {
    $module_images_path = drupal_get_path('module', 'commerce_tenpay') . '/images';
    foreach ($element['#options'] as $key => $choice) {
      if ($key != 0) {
        $element[$key]['#title'] = '<span class="bank-logo bank-'.$key.'">'. $choice .'</span>';
      }
      else {
        $element[$key]['#title'] = '<span class="bank-logo-default">' . $choice . '</span>';
      }
    }
  }
  return $element;
}
